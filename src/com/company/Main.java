package com.company;

import com.company.Exceptions.TestFailed;

import java.text.DateFormat;
import java.util.*;

public class Main {


    public static void main(String[] args) {
        try {
            Main.test_methodOfPerson_fullYearsDiff();
            System.out.println("You are here and this means I am ready.");

            Locale locale = Locale.ENGLISH; // en
            //Locale locale = new Locale("ru"); // ru  - not fully adopted
            String inLocale = locale.toString().equals("ru") ? "Русский язык ввода/вывода." : "Locale is " + locale.toString() + ".";
            System.out.println(inLocale);

            TimeZone tz = TimeZone.getTimeZone("GMT+3");
            System.out.println("TimeZone is " + tz.getDisplayName());

            Calendar today = Calendar.getInstance(tz, locale);
            DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, locale);
           // DateFormat tf = DateFormat.getTimeInstance(DateFormat.FULL, locale);
            df.setTimeZone(tz);
            //tf.setTimeZone(tz);
            System.out.println("Today is " + df.format(today.getTime()) + " by Gregorian Calendar.");
            //System.out.println("Today is " + df.format(today.getTime()) + " at " + tf.format(today.getTime()) + " by Gregorian Calendar.");


            System.out.println("I am working in these time zone and calendar.");

            boolean again = true;
            do {
                try {
                    wouldYouLikeToContinue();
                    Scanner in_c = new Scanner(System.in);
                    int c = in_c.nextInt();
                    start:
                    switch (c) {
                        case 1:
                            System.out.println("Ok. We are continuing.");
                            Person person = Person.createPersonFromConsole(tz, locale);
                            System.out.println(person.toString(locale));
                            System.out.print(person.toStringWithInitials());
                            System.out.print((locale.toString().equals("ru") ? " " : " is ") + person.gender(locale));
                            System.out.print((locale.toString().equals("ru") ? " " : " and ") + (person.fullYearsFromNow(tz, locale)<0?"N/A":person.fullYearsFromNow(tz, locale)) + (locale.toString().equals("ru") ? " " : " years old."));
                            System.out.println();
                            break;
                        case 0:
                            again = false;
                            break;
                        default:
                            System.out.println("Congratulation! You have got the second chance! Please try again.");
                    }
                } catch (RuntimeException e) {
                    System.out.println("I have caught RuntimeException");
                    System.out.println(e.toString());
                    System.out.println("Congratulation! You have got the second chance! Please try again.");
                    again = true;
                } catch (Exception e) {
                    System.out.println("I have caught Exception");
                    System.out.println(e.toString());
                    System.out.println("Congratulation! You have got the second chance! Please try again.");
                    again = true;
                } catch (OutOfMemoryError e) {
                    System.out.println(e.toString());
                    System.out.println("Congratulation! You have got the second chance! Please try again.");
                    again = true;
                }
            } while (again);
            System.out.println("Ok. You have wished to exit.");
        } catch (TestFailed e) {
            System.out.println(e.toString());
            System.out.println("I am NOT ready.");
        } catch (RuntimeException e) {
            System.out.println("I have get RuntimeException");
            System.out.println(e.toString());
        } catch (Exception e) {
            System.out.println("I have get Exception");
            System.out.println(e.toString());
        } catch (Error e) {
            System.out.println("I have get Error");
            System.out.println(e.toString());
        } finally {
            System.out.println("\n" + "See you again!");
        }
    }

    public static void wouldYouLikeToContinue() {
        System.out.println("Would you like to continue?");
        System.out.println("[1] to continue");
        System.out.println("[0] to exit");
        System.out.println("Please do your choose and enter the number:");
    }

    public static String getMethodName() {
        return Thread.currentThread().getStackTrace()[1].getClassName() + "." + Thread.currentThread().getStackTrace()[2].getMethodName();
    }

    public static void test_methodOfPerson_fullYearsDiff() throws Exception {
        String msg = getMethodName() + "\t\t";
        if (Person.fullYearsDiff("2017-04-15", "2017-04-15") != 0) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2017-04-16") != -1) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2017-04-14") != 0) throw new TestFailed(msg);

        if (Person.fullYearsDiff("2017-04-15", "2017-03-15") != 0) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2017-03-16") != 0) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2017-03-14") != 0) throw new TestFailed(msg);

        if (Person.fullYearsDiff("2017-04-15", "2016-04-15") != 1) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2016-04-16") != 0) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-04-15", "2016-04-14") != 1) throw new TestFailed(msg);

        if (Person.fullYearsDiff("2017-03-01", "2016-02-29") != 1) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2017-02-28", "2016-02-29") != 0) throw new TestFailed(msg);
        if (Person.fullYearsDiff("2020-02-29", "2016-02-29") != 4) throw new TestFailed(msg);

        System.out.println(new TestPassed(msg).toString());
    }
}
