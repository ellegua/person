package com.company;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Person {
    private String lastName;
    private String firstName;
    private String middleName;
    private Calendar birthday;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    Person(String lastName, String firstName, String middleName, Calendar birthday) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.birthday = birthday;
    }

    Person() {
        this("", "", "", Calendar.getInstance());
    }

    public static int fullYearsDiff(Calendar nowCalendar, Calendar birthdayCalendar) {

        int fullYears = -1;
        int diffNowAndBirthday = nowCalendar.compareTo(birthdayCalendar);
        int diffYearNowAndBirthday = nowCalendar.get(Calendar.YEAR) - birthdayCalendar.get(Calendar.YEAR);
        int diffMonthOfYearNowAndBirthday = nowCalendar.get(Calendar.MONTH) - birthdayCalendar.get(Calendar.MONTH);
        int diffDayOfMonthNowAndBirthday = nowCalendar.get(Calendar.DAY_OF_MONTH) - birthdayCalendar.get(Calendar.DAY_OF_MONTH);

        if (diffNowAndBirthday < 0) {
            //System.out.println("Unfortunately, You are not with us yet.");
        } else {
            if (diffNowAndBirthday == 0) {
                //System.out.println("Welcome!");
                fullYears = diffYearNowAndBirthday;
            } else if (diffMonthOfYearNowAndBirthday > 0 || (diffMonthOfYearNowAndBirthday == 0 && diffDayOfMonthNowAndBirthday >= 0)) {
                if (diffMonthOfYearNowAndBirthday == 0 && diffDayOfMonthNowAndBirthday == 0) {
                    //System.out.println("Happy BD!");
                }
                fullYears = diffYearNowAndBirthday;
            } else {
                fullYears = diffYearNowAndBirthday - 1;
            }
            //System.out.println("Your are " + fullYears + " years old.");
        }
        return fullYears;
    }

    public static int fullYearsDiff(String now, String birthday) throws Exception {
        TimeZone tz = TimeZone.getTimeZone("GMT+3");
        Locale locale = Locale.ENGLISH; // en
        return Person.fullYearsDiff(Person.stringToCalendar(now, tz, locale), Person.stringToCalendar(birthday, tz, locale));
    }

    public int fullYearsFromNow(TimeZone tz, Locale locale) {

        Calendar now = Calendar.getInstance(tz, locale);
        return fullYearsDiff(now, birthday);
    }

    public static String dateToString(Date date, Locale locale) {
        //String pattern="EEE, MMM d, yyyy z";
        String pattern = "EEEE, MMM d, yyyy z";
        SimpleDateFormat outDateFormat = new SimpleDateFormat(pattern, locale); //hard-code
        return outDateFormat.format(date);
    }

    public static String calendarToString(Calendar calendar, Locale locale) {
        Date date = calendar.getTime();
        return dateToString(date, locale);
    }

    public String birthdayToString(Locale locale) {
        return calendarToString(birthday, locale);
    }

    public static Date stringToDate(String dateString, TimeZone tz, Locale locale) throws Exception {
        DateFormat inDateFormat = new SimpleDateFormat("yyyy-MM-dd", locale); //hard-code
        inDateFormat.setTimeZone(tz);
        Date dateFormatted = inDateFormat.parse(dateString);
        return dateFormatted;
    }

    public static Calendar stringToCalendar(String calendarString, TimeZone tz, Locale locale) throws Exception {
        Date date = stringToDate(calendarString, tz, locale);
        Calendar calendar = Calendar.getInstance(tz, locale);
        calendar.setTime(date);
        return calendar;
    }

    public String toString(Locale locale) {
        return lastName + " " + firstName + " " + middleName + " " + birthdayToString(locale);
    }

    public static Person createPersonFromConsole(TimeZone tz, Locale locale) throws Exception { // check inputted strings
        if (locale.toString().equals("ru")) {
            System.out.println("Введите фамилию, имя и отчество через пробельные символы и дату рождения в формате гггг-ММ-дд");
        } else {
            System.out.println("Input last name, first name and middle name separated by space and birthday date in the following format yyyy-MM-dd");
        }
        Scanner in = new Scanner(System.in);
        String lastName = in.next();
        String firstName = in.next();
        String middleName = in.next();
        String birthday = in.next();
        return new Person(lastName, firstName, middleName, stringToCalendar(birthday, tz, locale));
    }

    public static String initial(String name) {
        return name.charAt(0) + ".";
    }

    public String toStringWithInitials() {
        return lastName + " " + initial(firstName) + initial(middleName);
    }

    public String gender(Locale locale) {
        String mileGender = locale.toString().equals("ru") ? "муж" : "male";
        String femaleGender = locale.toString().equals("ru") ? "жен" : "female";
        String gender = mileGender;
        if (middleName.endsWith("a") || middleName.endsWith("а")) gender = femaleGender;
        return gender;
    }
}
